package uz.openweb.vendor;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;


public class NotificationService extends FirebaseMessagingService {

    private String type;
    private String reason = "";
    private String news_id = "";
    private String CHANNEL_ID = "";
    private String CHANNEL_NAME = "Default";
    private String CHANNEL_DESC = "Default for notification";

    private String refresh_token = "";


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) Log.d("", "Message Notification Body: " + remoteMessage.getNotification().getBody());

        Log.d("remote message: ", " " + remoteMessage.getNotification());
        Log.d("from message: ", "From: " + remoteMessage.getFrom());
        String title = "";
        String message = "";

        if (remoteMessage.getData().size() > 0) {
            Log.d("", "Message data payload: " + remoteMessage.getData());

            Map<String, String> data = remoteMessage.getData();
            type = data.get("type");
            if (data.get("reason") != null) reason = data.get("reason");
            if (data.get("news_id") != null) news_id = data.get("news_id");
        }

    }


    private void sendNotification(String title, String message, long[] vibrate, Uri sound) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.raw.notify);
//        Uri soundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.sound);
//        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_baseline_notifications_24); //replace with your own image



        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int notificationImportance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, notificationImportance);
            channel.setDescription(CHANNEL_DESC);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.setSound(soundUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            channel.enableVibration(true);
            if (notificationManager != null) notificationManager.createNotificationChannel(channel);
        }

        int notificationPriority = NotificationCompat.PRIORITY_HIGH;
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.launch_background)
//                .setLargeIcon(largeIcon)
//                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(largeIcon).bigLargeIcon(null))
                .setContentTitle(title)
                .setContentText(message)
                .setSound(soundUri)
//                .setColor(getResources().getColor(R.color.colorAccent))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setPriority(notificationPriority);

//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        assert notificationManager != null;
        notificationManager.notify(getRequestCode(), notificationBuilder.build());
    }

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }
}
