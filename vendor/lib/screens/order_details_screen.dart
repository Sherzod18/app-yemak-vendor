import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../providers/order.dart' show Order;
import '../models/root.dart';

class OrderDetailsScreen extends StatefulWidget {
  static const routeName = '/order-details';

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  var minIndex = 0;
  var _expanded = true;
  var _name = '';
  var _isNameLoading = false;
  var _isMainLoading = false;
  var _index = 99;
  int orderId;
  var _isInit = true;
  Root order;
  var _isLoading = false;
  FToast fToast;

  var obj = ['Mahsulot tugab qoldi', 'Ish vaqti tugadi', 'Boshqa'];

  @override
  void initState() {
    _expanded = true;
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  void didChangeDependencies() {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, int>;
    orderId = routeArgs['id'];

    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      order = Provider.of<Order>(context, listen: false).getById(orderId);
      Timer(Duration(milliseconds: 500), () {
        setState(() {
          _isLoading = false;
        });
      });
    }

    _isInit = false;

    super.didChangeDependencies();
  }

  Widget getContainer(Root root) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
//              border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
          ),
        ],
      ),
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
      padding: EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height * 0.6,
      width: MediaQuery.of(context).size.width - 32,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 8),
            child: Text(
              'Buyurtma #${root.number}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                'Maxsulotlar',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
              )),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: MediaQuery.of(context).size.width * 0.9,
//                    margin: EdgeInsets.all(4),
            child: ListView.builder(
              itemBuilder: (ctx, index) => Column(
                children: [
                  ListTile(
                    leading: Text(
                      'x${root.products[index].amount}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${root.products[index].name}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            '${root.products[index].price} so\'m',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Text(
                      '${root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().substring(1, root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().length - 1)}',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Divider(
                    height: 10,
                  ),
                ],
              ),
              itemCount: root.products.length,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Text(
                      'Jami narxi: ${root.cost_of_products} so\'m',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        Text(
                          'To\'lov: ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          ' naqt pul',
                          style: TextStyle(color: Colors.black38),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    final orderId =
//        ModalRoute.of(context).settings.arguments as Map<String, int>;

    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  getContainer(order),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(right: 5),
                            height: 45,
                            child: RaisedButton(
                              color: Color(0xFFFFE030),
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              onPressed: openAlertBox,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              child: Text(
                                'Qabul qilish',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 45,
                            margin: EdgeInsets.only(left: 5),
                            child: RaisedButton(
                              color: Color(0xFFD8D8D8),
                              onPressed: openAlertCancel,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              child: Text(
                                'Bekor qilish',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 16,
                      left: 16,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 45,
                            child: RaisedButton(
                              color: Color(0xFF206A5D),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              onPressed: () {},
                              child: Text(
                                'Operator bilan bog\'lanish',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  openAlertBox() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              contentPadding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              backgroundColor: Color(0xFFF2F2F2),
              content: Container(
                width: MediaQuery.of(context).size.width - 32,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Tayyor bo\'lish vaqtini tanlang:',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          CloseButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(right: 20, left: 10, bottom: 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 15
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(15);
                                              });
                                      },
                                      color: minIndex == 15
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('15 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 20
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(20);
                                              });
                                      },
                                      color: minIndex == 20
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('20 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 25
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(25);
                                              });
                                      },
                                      color: minIndex == 25
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('25 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 30
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(30);
                                              });
                                      },
                                      color: minIndex == 30
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('30 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 40
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(40);
                                              });
                                      },
                                      color: minIndex == 40
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('40 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 50
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(50);
                                              });
                                      },
                                      color: minIndex == 50
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('50 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 60
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(60);
                                              });
                                      },
                                      color: minIndex == 60
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('1 soat'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 22
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(22);
                                              });
                                      },
                                      color: minIndex == 22
                                          ? Color(0xFFFFE030)
                                          : Color(0xFF206A5D),
                                      child: Text(
                                        'Tayyor',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (minIndex != 0 && !_isMainLoading) {
                            setState(() {
                              _isMainLoading = true;
                            });
                            Provider.of<Order>(context, listen: false)
                                .acceptOrder(orderId, minIndex)
                                .then((value) {
//                              Navigator.of(context).pop();
                              Provider.of<Order>(context, listen: false)
                                  .getAllOrders()
                                  .then((value) {
                                setState(() {
                                  minIndex = 0;
                                  _isMainLoading = false;
                                });
                                Navigator.of(context)
                                    .popUntil((route) => route.isFirst);
                                _showToast();
                              });
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          decoration: BoxDecoration(
                            color: (minIndex != 0 && !_isMainLoading)
                                ? Color(0xFFFFE030)
                                : Color(0xFFD8D8D8), //Color(0xFFD8D8D8),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5.0),
                                bottomRight: Radius.circular(5.0)),
                          ),
                          child: Column(
                            children: [
                              _isMainLoading
                              ? CircularProgressIndicator()
                              : Text(
                                "Davom etish",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  openAlertCancel() {
    return showDialog(
      context: context,
//      barrierDismissible: false,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              contentPadding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              backgroundColor: Color(0xFFF2F2F2),
              content: Container(
                width: MediaQuery.of(context).size.width - 32,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Sabab:',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          CloseButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        margin: EdgeInsets.only(bottom: 30),
//                        height: 100,
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(4),
                              ),
                              height: 50,
//                            padding: EdgeInsets.only(top: 10, bottom: 10),
                              child: Center(
                                child: ListTile(
                                  onTap: () {
                                    setState(() {
                                      _index = 99;
                                      _name = '';
                                      _expanded = !_expanded;
                                    });
                                  },
                                  title: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        _index != 99
                                            ? _name
                                            : 'Sababni ko\'rsating...',
                                        style: TextStyle(
                                            color: _index != 99
                                                ? Colors.black
                                                : Colors.black38),
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black38,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            AnimatedContainer(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.only(left: 8, right: 8),
                              duration: Duration(milliseconds: 300),
                              height: _expanded ? 0 : 180,
                              child: ListView.builder(
                                itemBuilder: (ctx, index) => Column(
                                  children: [
                                    Container(
                                      height: 40,
                                      child: ListTile(
                                        onTap: () {
                                          print('----------------');
                                          setState(() {
                                            _name = obj[index];
                                            _index = index;
                                            _expanded = !_expanded;
                                          });
                                        },
                                        title: Text(obj[index]),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                                itemCount: obj.length,
                              ),
                            ),
                            AnimatedContainer(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.only(left: 8, right: 8),
                              duration: Duration(milliseconds: 300),
                              height: _index == 2 ? 100 : 0,
                              child: TextField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Sababni kiriting'),
                                onChanged: (value) {
                                  setState(() {
                                    _name = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (_name.length > 7 && !_isNameLoading) {
                            setState(() {
                              _isNameLoading = true;
                            });
                            Provider.of<Order>(context, listen: false)
                                .rejectOrder(orderId.toString(), _name)
                                .then((value) {
//                              Navigator.of(context).pop();
                              Provider.of<Order>(context, listen: false)
                                  .getAllOrders()
                                  .then((value) {
                                setState(() {
                                  _name = '';
                                  _isNameLoading = false;
                                });
                                Navigator.of(context)
                                    .popUntil((route) => route.isFirst);
                                _showToastReject();
                              });
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          decoration: BoxDecoration(
                            color: (_name.length > 7 && !_isNameLoading)
                                ? Color(0xFFFFE030)
                                : Color(0xFFD8D8D8), //Color(0xFFD8D8D8),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5.0),
                                bottomRight: Radius.circular(5.0)),
                          ),
                          child: Column(
                            children: [
                              _isNameLoading
                                  ? CircularProgressIndicator()
                                  : Text(
                                      "Bekor qilish",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                      textAlign: TextAlign.center,
                                    )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  showToast() {
    Fluttertoast.showToast(
        msg: "This is Toast messaget",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1);
  }

  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Buyurtma qabul qilindi"),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
//    fToast.showToast(
//        child: toast,
//        toastDuration: Duration(seconds: 2),
//        positionedToastBuilder: (context, child) {
//          return Positioned(
//            child: child,
//            top: 16.0,
//            left: 16.0,
//          );
//        });
  }

  _showToastReject() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.red[300],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Buyurtma bekor qilindi"),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
//    fToast.showToast(
//        child: toast,
//        toastDuration: Duration(seconds: 2),
//        positionedToastBuilder: (context, child) {
//          return Positioned(
//            child: child,
//            top: 16.0,
//            left: 16.0,
//          );
//        });
  }

  void _click(int clickIndex) {
    setState(() {
      minIndex = clickIndex;
    });
  }
}
