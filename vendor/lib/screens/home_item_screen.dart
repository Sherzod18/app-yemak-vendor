import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/order.dart' show Order;
import '../models/root.dart';

class HomeItemScreen extends StatefulWidget {
  static const routeName = '/home-item';

  @override
  _HomeItemScreenState createState() => _HomeItemScreenState();
}

class _HomeItemScreenState extends State<HomeItemScreen> {
  var minIndex = 0;
  int orderId;
  Root order;
  var _isLoading = false;
  var _isMinLoading = false;
  var _isInit = true;
  var _isLoading2 = false;
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  void _submit() {
    setState(() {
      _isLoading2 = true;
    });
    Provider.of<Order>(context, listen: false).orderReady(orderId).then((_) {
      Provider.of<Order>(context, listen: false).getAllAcceptOrders().then((_) {
        setState(() {
          _isLoading2 = false;
        });
        _showToast();
        Navigator.of(context).pop();
      });
    });
  }

  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Buyurtma tayyor"),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  Widget getContainer(BuildContext context, Root root) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
//              border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
          ),
        ],
      ),
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
      padding: EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height * 0.6,
      width: MediaQuery.of(context).size.width - 32,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 8),
            child: Text(
              'Buyurtma #${root.number}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                'Maxsulotlar',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
              )),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: MediaQuery.of(context).size.width * 0.9,
//                    margin: EdgeInsets.all(4),
            child: ListView.builder(
              itemBuilder: (ctx, index) => Column(
                children: [
                  ListTile(
                    leading: Text(
                      'x${root.products[index].amount}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${root.products[index].name}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            '${root.products[index].price} so\'m',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Text(
                      '${root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().substring(1, root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().length - 1)}',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Divider(
                    height: 10,
                  ),
                ],
              ),
              itemCount: root.products.length,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Text(
                      'Jami narxi: ${root.cost_of_products} so\'m',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        Text(
                          'To\'lov: ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          ' naqt pul',
                          style: TextStyle(color: Colors.black38),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

// timestamp ni vaqtga ugirish methodi
  String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {
        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }

    return time;
  }

  @override
  void didChangeDependencies() {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, int>;
    orderId = routeArgs['id'];

    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      setState(() {
        order =
            Provider.of<Order>(context, listen: false).getByIdAccept(orderId);
      });

//      Timer(Duration(milliseconds: 300), () {
//        setState(() {
//          _isLoading = false;
//        });
//      });

      setState(() {
        _isLoading = false;
      });
    }

    _isInit = false;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                children: [
                  getContainer(context, order),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 16,
                      left: 16,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 45,
                            child: RaisedButton(
                              color: Color(0xFF206A5D),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              onPressed: openAlertBox,
                              child: Text(
                                'Tanlangan vaqt ${readTimestamp(order.cooking_time)}',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  if (_isLoading2)
                    CircularProgressIndicator()
                  else
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 16,
                        left: 16,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 45,
                              child: RaisedButton(
                                color: Color(0xFFFFE030),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0))),
                                onPressed: _submit,
                                child: Text(
                                  'Buyurtma tayyor',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                ],
              ),
            ),
    );
  }

  openAlertBox() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              contentPadding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              backgroundColor: Color(0xFFF2F2F2),
              content: Container(
                width: MediaQuery.of(context).size.width - 32,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Tayyor bo\'lish vaqtini tanlang:',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          CloseButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(right: 20, left: 10, bottom: 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 10
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(10);
                                              });
                                      },
                                      color: minIndex == 10
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('10 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 15
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(15);
                                              });
                                      },
                                      color: minIndex == 15
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('15 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 20
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(20);
                                              });
                                      },
                                      color: minIndex == 20
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('20 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 25
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(25);
                                              });
                                      },
                                      color: minIndex == 25
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('25 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 30
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(30);
                                              });
                                      },
                                      color: minIndex == 30
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('30 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 40
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(40);
                                              });
                                      },
                                      color: minIndex == 40
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('40 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 50
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(50);
                                              });
                                      },
                                      color: minIndex == 50
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('50 min'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: RaisedButton(
                                      onPressed: () {
                                        minIndex == 60
                                            ? setState(() {
                                                _click(0);
                                              })
                                            : setState(() {
                                                _click(60);
                                              });
                                      },
                                      color: minIndex == 60
                                          ? Color(0xFFFFE030)
                                          : Color(0xFFFFFFFF),
                                      child: Text('1 soat'),
                                      padding:
                                          EdgeInsets.only(left: 0, right: 0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (minIndex != 0 && !_isMinLoading) {
                            setState(() {
                              _isMinLoading = true;
                            });
                            Provider.of<Order>(context, listen: false)
                                .editedTime(
                                    orderId, minIndex, order.scheduled_time)
                                .then((value) {
                              Provider.of<Order>(context, listen: false)
                                  .getAllAcceptOrders()
                                  .then((value) {
                                setState(() {
                                  order =
                                      Provider.of<Order>(context, listen: false)
                                          .getByIdAccept(orderId);
                                  minIndex = 0;
                                  _isMinLoading = false;
                                });
//                                Navigator.of(context).pop();
                                Navigator.of(context)
                                    .popUntil((route) => route.isFirst);
                              });
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          decoration: BoxDecoration(
                            color: (minIndex != 0 && !_isMinLoading)
                                ? Color(0xFFFFE030)
                                : Color(0xFFD8D8D8), //Color(0xFFD8D8D8),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5.0),
                                bottomRight: Radius.circular(5.0)),
                          ),
                          child: Column(
                            children: [
                              _isMinLoading
                                  ? CircularProgressIndicator()
                                  : Text(
                                      "Vaqtni o'zgartirish",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                      textAlign: TextAlign.center,
                                    ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  void _click(int clickIndex) {
    setState(() {
      minIndex = clickIndex;
    });
  }
}
