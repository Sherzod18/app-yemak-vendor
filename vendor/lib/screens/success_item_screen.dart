import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../dummy_data.dart';
import '../models/order_detail.dart';
import '../models/root.dart';
import '../providers/order.dart';

class SuccessItemScreen extends StatefulWidget {
  static const routeName = '/success-item';
  @override
  _SuccessItemScreenState createState() => _SuccessItemScreenState();
}

class _SuccessItemScreenState extends State<SuccessItemScreen> {
  var minIndex = 0;
  int orderId;
  Root order;
  var _isLoading = false;
  var _isInit = true;
  var _isLoading2 = false;
  FToast fToast;

  List<OrderDetail> _orderDetails;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  void _submit(){
    setState(() {
      _isLoading2 = true;
    });
    Provider.of<Order>(context, listen: false).orderReady(orderId).then((_){
      Provider.of<Order>(context, listen: false).getAllSuccessOrders().then((_) {
        setState(() {
          _isLoading2 = false;
        });
        _showToast();
        Navigator.of(context).pop();
      });
    });
  }
  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Haydovchi chaqirildi"),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }


  Widget getContainer(BuildContext context, Root root) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
//              border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
          ),
        ],
      ),
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
      padding: EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height * 0.6,
      width: MediaQuery.of(context).size.width - 32,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 8),
            child: Text(
              'Buyurtma #${root.number}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                'Maxsulotlar',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
              )),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: MediaQuery.of(context).size.width * 0.9,
//                    margin: EdgeInsets.all(4),
            child: ListView.builder(
              itemBuilder: (ctx, index) => Column(
                children: [
                  ListTile(
                    leading: Text(
                      'x${root.products[index].amount}',
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${root.products[index].name}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            '${root.products[index].price} so\'m',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Text(
                      '${root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().substring(1, root.products[index].options.map((item) => '${item.name}: ${item.row.name}').toString().length - 1)}',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Divider(
                    height: 10,
                  ),
                ],
              ),
              itemCount: root.products.length,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Text(
                      'Jami narxi: ${root.cost_of_products} so\'m',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        Text(
                          'To\'lov: ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          ' naqt pul',
                          style: TextStyle(color: Colors.black38),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void didChangeDependencies() {
    final routeArgs =
    ModalRoute.of(context).settings.arguments as Map<String, int>;
    orderId = routeArgs['id'];

    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      order = Provider.of<Order>(context, listen: false).getByIdSuccess(orderId);
//      Timer(Duration(milliseconds: 300), () {
//        setState(() {
//          _isLoading = false;
//        });
//      });

      setState(() {
        _isLoading = false;
      });
    }

    _isInit = false;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            getContainer(context,order),
            SizedBox(
              height: 15,
            ),
            if(_isLoading2)
              CircularProgressIndicator()
            else
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 45,
                      child: RaisedButton(
                        color: Color(0xFFFFE030),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(5.0))),
                        onPressed: _submit,
                        child: Text(
                          'Haydovchini chaqirish',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
