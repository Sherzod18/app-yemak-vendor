import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './home_item_screen.dart';
import '../providers/order.dart' show Order;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver{
  void selectCategory(BuildContext ctx, int id) {
    Navigator.of(ctx).pushNamed(
      HomeItemScreen.routeName,
      arguments: {
        'id': id,
      },
    );
  }

  Future<void> _refreshProducts(BuildContext context) async {
    print('Ishladi -------------');
    await Provider.of<Order>(context, listen: false).getAllAcceptOrders();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: Provider.of<Order>(context, listen: false).getAllAcceptOrders(),
      builder: (ctx, snapshot) =>
          snapshot.connectionState == ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Consumer<Order>(
                    builder: (ctx, orderData, child) => ListView.builder(
                      itemCount: orderData.acceptOrders.length,
                      itemBuilder: (_, i) => Container(
                        width: size.width - 32,
                        height: 50,
                        margin: EdgeInsets.only(top: 10, left: 16, right: 16),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 5.0,
                            ),
                          ],
                        ),
                        child: ListTile(
                          onTap: () =>
                              selectCategory(context, orderData.acceptOrders[i].id),
                          title: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Buyurtma #${orderData.acceptOrders[i].number}',
                                style: TextStyle(
                                    fontWeight:
                                    FontWeight.bold),
                              ),
                              Text(
                                '${orderData.acceptOrders[i].cost_of_products} s.',
                                style: TextStyle(
                                    color: Colors.black38),
                              ),
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
    );
  }
}
