import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import './order_history_item_screen.dart';
import '../models/root.dart';
import '../providers/order.dart';

class OrderHistoryScreen extends StatefulWidget {
  static const routeName = '/order-history';

  @override
  _OrderHistoryScreenState createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen> {
  var _isLoading = false;
  var _isInit = true;
  List<Root> roots;
  String date1 = "";

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      Provider.of<Order>(context, listen: false).getHistoryOrders().then((_) {
        roots = Provider.of<Order>(context, listen: false).historyOrders;
        setState(() {
          _isLoading = false;
        });
      });
    }

    _isInit = false;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    /*if (!_isLoading) {
      print("----------------------------");
      DateTime now = DateTime.now();
      roots.forEach((item) {
        var date = DateTime.fromMillisecondsSinceEpoch(item.created_at * 1000);
        String formattedDate = DateFormat('dd-MM-yyyy').format(date);

        if (formattedDate == date1) {
          print(item.id);
        } else {
          print("============ $formattedDate ==============");
          date1 = formattedDate;
          print(item.id);
        }
        print(item.created_at);
      });
      print("----------------------------");
    }*/

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: _isLoading ? Center(
        child: CircularProgressIndicator(),
      ): SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
            child: Column(
              children: roots.map((item){

                var date = DateTime.fromMillisecondsSinceEpoch(item.created_at * 1000);
                String formattedDate = DateFormat('dd-MM-yyyy').format(date);
                if (formattedDate == date1) {
                  print(item.id);
                  return Container(
                    width: size.width - 32,
                    height: 50,
                    margin: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 5.0,
                        ),
                      ],
                    ),
                    child: ListTile(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(OrderHistoryItemScreen.routeName);
                      },
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Buyurtma #${item.number}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '${item.cost_of_products} s.',
                            style: TextStyle(color: Colors.black38),
                          ),
                        ],
                      ),
                    ),
                  );
                } else {
                  print("============ $formattedDate ==============");
                  date1 = formattedDate;
                  print(item.id);
                  return Column(children: [
                    SizedBox(height: 10,),
                    Center(
                      child: Text(
                        '$formattedDate',
                        style: TextStyle(color: Colors.black38),
                      ),
                    ),
                    Container(
                      width: size.width - 32,
                      height: 50,
                      margin: EdgeInsets.only(top: 15),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: 5.0,
                          ),
                        ],
                      ),
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(OrderHistoryItemScreen.routeName);
                        },
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Buyurtma #${item.number}',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${item.cost_of_products} s.',
                              style: TextStyle(color: Colors.black38),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],);
                }
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }
}
