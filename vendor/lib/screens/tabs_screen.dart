import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import './restaurant_screen.dart';
import './home_screen.dart';
import './orders_screen.dart';
import './success_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List<Map<String, Object>> _pages = [
    {'page': OrdersScreen(), 'title': 'Yangi'},
    {'page': HomeScreen(), 'title': 'Tayyorlanmoqda'},
    {'page': SuccessScreen(), 'title': 'Tayyor'},
    {'page': RestaurantScreen(), 'title': 'Restaurant'},
  ];

  int _selectedItemIndex = 0;

  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  @override
  void initState() {
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
//      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        print(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        print(message);
      },
    );

    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed ============= _TabsScreenState');
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    print(' +++++++++ didChangeDependencies ++++++++++++');

    firebaseMessaging.configure(onLaunch: (Map<String, dynamic> msg) {
      print(" onLaunch called --------- _TabsScreenState");
    }, onResume: (Map<String, dynamic> msg) {
      print(" onResume called ----------- _TabsScreenState");
    }, onMessage: (Map<String, dynamic> msg) {
      print(" onMessage called ----------- _TabsScreenState");
    });

    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed ============= _TabsScreenState');
    });

    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(TabsScreen oldWidget) {
    print(' +++++++++ didUpdateWidget ++++++++++++');

    firebaseMessaging.configure(onLaunch: (Map<String, dynamic> msg) {
      print(" onLaunch called --------- _TabsScreenState");
    }, onResume: (Map<String, dynamic> msg) {
      print(" onResume called ----------- _TabsScreenState");
    }, onMessage: (Map<String, dynamic> msg) {
      print(" onMessage called ----------- _TabsScreenState");
    });

    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed ============= _TabsScreenState');
    });

    super.didUpdateWidget(oldWidget);
  }

//  int _selectedPageIndex = 0;

//  void _selectPage(int index){
//    setState(() {
//      _selectedPageIndex = index;
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
//        color: Colors.white,
        decoration: new BoxDecoration(
          color: const Color(0xFFFFFFFF),
          boxShadow: [
            new BoxShadow(
              color: Colors.black12,
              blurRadius: 5.0,
            ),
          ],
        ),
        child: Row(
          children: [
            buildNavBarItem(Icons.home, 0, 'Yangi'),
            buildNavBarItem(Icons.schedule, 1, 'Tayyorlanmoqda'),
            buildNavBarItem(Icons.check, 2, 'Tayyor'),
            buildNavBarItem(Icons.restaurant, 3, 'Restaurant'),
          ],
        ),
      ),
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          CircleAvatar(
            backgroundColor: Colors.white,
            child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
          ),
        ],
      ),
      body: _pages[_selectedItemIndex]['page'],
    );
  }

  Widget buildNavBarItem(IconData icon, int index, String text) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedItemIndex = index;
        });
      },
      child: Container(
        padding: index == _selectedItemIndex
            ? EdgeInsets.only(top: 4)
            : EdgeInsets.only(top: 6),
        height: 60,
        width: MediaQuery.of(context).size.width / 4,
        decoration: index == _selectedItemIndex
            ? BoxDecoration(
                border: Border(
                  top: BorderSide(width: 2, color: Color(0xFFFFA802)),
                ),
              )
            : BoxDecoration(),
        child: Column(
          children: [
            SizedBox(
              height: 4,
            ),
//            SvgPicture.asset(
//              'assets/images/${icon}',
//              width: 20,
//              height: 20,
//              fit: BoxFit.scaleDown,
//              color: index == _selectedItemIndex
//                  ? Color(0xFFFFA802)
//                  : Color(0xFFAAAAAA),
//            ),
            Icon(
              icon,
              color: index == _selectedItemIndex
                  ? Color(0xFFFFA802)
                  : Color(0xFFAAAAAA),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              text,
              style: TextStyle(
                fontSize: 11,
                color: index == _selectedItemIndex
                    ? Color(0xFFFFA802)
                    : Color(0xFFAAAAAA),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
