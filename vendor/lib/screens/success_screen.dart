import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/success_item_screen.dart';
import '../providers/order.dart';

class SuccessScreen extends StatefulWidget {
  @override
  _SuccessScreenState createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen>  with WidgetsBindingObserver{
  void selectCategory(BuildContext ctx, int id) {
    Navigator.of(ctx).pushNamed(
      SuccessItemScreen.routeName,
      arguments: {
        'id': id,
      },
    );
  }

  Future<void> _refreshProducts(BuildContext context) async {
    print('Ishladi -------------');
    await Provider.of<Order>(context, listen: false).getAllSuccessOrders();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: Provider.of<Order>(context, listen: false).getAllSuccessOrders(),
      builder: (ctx, snapshot) => snapshot.connectionState ==
              ConnectionState.waiting
          ? Center(
              child: CircularProgressIndicator(),
            )
          : RefreshIndicator(
              onRefresh: () => _refreshProducts(context),
              child: Consumer<Order>(
                builder: (ctx, orderData, child) => ListView.builder(
                  itemCount: orderData.successOrders.length,
                  itemBuilder: (_, i) => Container(
                    width: size.width - 32,
                    height: 50,
                    margin: EdgeInsets.only(top: 10, left: 16, right: 16),
                    decoration: BoxDecoration(
                      color: Color(0xFF206A5D),
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 5.0,
                        ),
                      ],
                    ),
                    child: ListTile(
                      onTap: () =>
                          selectCategory(context, orderData.successOrders[i].id),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Buyurtma #${orderData.successOrders[i].number}',
                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          Text(
                            '${orderData.successOrders[i].cost_of_products} s.',
                            style: TextStyle(color: Colors.white30),
                          ),
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }

}
