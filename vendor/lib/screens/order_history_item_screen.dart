import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../dummy_data.dart';
import '../models/order_detail.dart';

// ignore: must_be_immutable
class OrderHistoryItemScreen extends StatelessWidget {
  static const routeName = 'order-history-item';

  List<OrderDetail> _orderDetails;


  Widget getContainer(BuildContext context, String id, double price, List<OrderDetail> orderDetails) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
//              border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
          ),
        ],
      ),
      margin: EdgeInsets.only(left: 16, right: 10, bottom: 10, top: 15),
      padding: EdgeInsets.all(15),
      height: size.height * 0.82,
      width: size.width - 32,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 8),
            child: Text(
              'Buyurtma #252525',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                'Maxsulotlar',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
              )),
          Container(
            height: size.height * 0.6,
            width: size.width - 32,
//                    margin: EdgeInsets.all(4),
            child: ListView.builder(
              itemBuilder: (ctx, index) => Column(
                children: [
                  ListTile(
                    leading: Text(
                      'x${(index + 1)}',
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '${DUMMY_ORDER_DETAILS[index].name}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          Text(
                            '${DUMMY_ORDER_DETAILS[index].price} so\'m',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Text(
                      DUMMY_ORDER_DETAILS[index]
                          .amounts
                          .map((item) => item)
                          .toString()
                          .substring(
                          1,
                          DUMMY_ORDER_DETAILS[index]
                              .amounts
                              .map((item) => item)
                              .toString()
                              .length -
                              1),
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Divider(
                    height: 10,
                  ),
                ],
              ),
              itemCount: DUMMY_ORDER_DETAILS.length,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  '01.01.2020 15:00',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'Jami narxi: 120.000 so\'m',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            getContainer(context, '111', 123, _orderDetails),
          ],
        ),
      ),
    );
  }
}
