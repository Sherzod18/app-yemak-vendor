import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/temporarily.dart';
import '../models/contact.dart';
import '../models/work_time.dart';
import '../models/restaurant.dart';
import './order_history_screen.dart';
import './products_screen.dart';
import '../providers/auth.dart';

class RestaurantScreen extends StatefulWidget {
  @override
  _RestaurantScreenState createState() => _RestaurantScreenState();
}

class _RestaurantScreenState extends State<RestaurantScreen>
    with WidgetsBindingObserver {
  var minIndex = 0;
  var closeTime = 0;
  var _isInit = true;
  var _isLoading = false;
  Restaurant restaurant;
  Contact contact;
  WorkTime workTime;
  Temporarily temporarily;

  @override
  initState() {
    super.initState();
    // Add listeners to this class

  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      Provider.of<Auth>(context, listen: false).getAccount().then((_) {
        restaurant = Provider.of<Auth>(context, listen: false).getRestaran;
        contact = Provider.of<Auth>(context, listen: false).getContact;
        workTime = Provider.of<Auth>(context, listen: false).getWorkTime;
        temporarily = Provider.of<Auth>(context, listen: false).getTemporarily;
        setState(() {
          _isLoading = false;
        });
      });
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(RestaurantScreen oldWidget) {

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  // ignore: non_constant_identifier_names
  Widget ContainerTime(int time) {
    var date = DateTime.fromMillisecondsSinceEpoch(time * 1000);
    String formattedDate = DateFormat('dd-MM-yyyy, HH:mm').format(date);
    if (time == -1) {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          'Restaran noma\'lum vaqtgacha yopilgan',
          style: TextStyle(color: Colors.redAccent),
        ),
      );
    } else if (time == 0) {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          'Ish vaqti: ${workTime.from.toString().substring(0, 5)} - ${workTime.to.toString().substring(0, 5)} (har kuni)',
          style: TextStyle(color: Colors.black38),
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          'Restaran shu $formattedDate vaqtgacha yopiq',
          style: TextStyle(color: Colors.black38),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Column(
            children: [
              SizedBox(
                height: size.height - 140,
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 0, right: 0),
                      height: size.height * 0.25,
                      child: CachedNetworkImage(
                        imageUrl: restaurant.photo,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin:
                          EdgeInsets.only(top: size.height * 0.18, left: 16),
                      height: size.height * 0.4,
                      width: size.width * 1 - 32,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10.0,
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                              '${restaurant.name}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 5, right: 5),
                            child: Wrap(
                              children: restaurant.tags.map((tag) {
                                return Container(
                                  child: Text(
                                    '$tag',
                                    style: TextStyle(
                                        fontSize: 12, color: Color(0xFFFFAA00)),
                                  ),
                                  padding: EdgeInsets.only(
                                    left: 6,
                                    right: 6,
                                    top: 4,
                                    bottom: 4,
                                  ),
                                  margin: EdgeInsets.only(right: 5, top: 5),
                                  decoration: BoxDecoration(
                                      color: Color(0xFFFFF6E5),
                                      borderRadius: BorderRadius.circular(20)),
                                );
                              }).toList(),
                            ),
                          ),
                          ContainerTime(temporarily.temporarily_closed_until),
//                          Container(
//                            padding: EdgeInsets.only(top: 10),
//                            child: Text(
//                              'Ish vaqti: ${workTime.from.toString().substring(0, 5)} - ${workTime.to.toString().substring(0, 5)} (har kuni)',
//                              style: TextStyle(color: Colors.black38),
//                            ),
//                          ),
                          Divider(
                            height: 10,
                          ),
                          Container(
                            height: 35,
                            child: ListTile(
                              leading: Icon(
                                Icons.update,
                                color: Color(0xFF000000),
                              ),
                              title: Text('Buyurtmalar tarixi'),
                              trailing: Icon(
                                Icons.arrow_forward_ios,
                                color: Color(0xFF989898),
                              ),
                              onLongPress: () {
                                print('salom');
                              },
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed(OrderHistoryScreen.routeName);
                              },
                            ),
                          ),
                          Container(
                            height: 40,
                            child: ListTile(
                              leading: Icon(
                                Icons.toc,
                                color: Color(0xFF000000),
                              ),
                              title: Text('Mahsulotlar'),
                              trailing: Icon(
                                Icons.arrow_forward_ios,
                                color: Color(0xFF989898),
                              ),
                              onLongPress: () {
                                print('salom');
                              },
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed(ProductsScreen.routeName);
                              },
                            ),
                          ),
                          Container(
                            height: 40,
                            child: ListTile(
                              leading: Icon(
                                Icons.exit_to_app,
                                color: Color(0xFF000000),
                              ),
                              title: Text('Akkountdan chiqish'),
                              trailing: Icon(
                                Icons.arrow_forward_ios,
                                color: Color(0xFF989898),
                              ),
                              onLongPress: () {
                                print('salom');
                              },
                              onTap: () {
                                print('Salom salom');
                                Navigator.of(context).pushReplacementNamed('/');

                                Provider.of<Auth>(context, listen: false)
                                    .logout();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin:
                          EdgeInsets.only(top: size.height * 0.62, left: 16),
                      width: size.width * 1 - 32,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
//                    FloatingActionButton(
//                      onPressed: () {
//                        openAlertBoxConnect(context);
//                      },
//                      child: Icon(Icons.phone),
//                      backgroundColor: Colors.white,
//                    ),

//                    FloatingActionButton(
//                      onPressed: () {},
//                      child: Icon(Icons.pause),
//                      backgroundColor: Colors.white,
//                    ),
                          InkWell(
                            onTap: () {
                              openAlertBoxConnect(context);
                            },
                            hoverColor: Colors.red,
                            child: Container(
                              width: 70,
                              height: 70,
                              child: Icon(
                                Icons.phone,
                                size: 35,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5.0,
                                  ),
                                ],
                              ),
                            ),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          InkWell(
                            onTap: () {
//                        setState(() {
//                          _isIcon = true;
//                        });
                              openAlertBoxClose(context);
                            },
                            hoverColor: Colors.red,
                            child: Container(
                              width: 70,
                              height: 70,
                              child: Icon(
                                temporarily.temporarily_closed_until == 0
                                    ? Icons.pause
                                    : Icons.play_arrow,
                                size: 35,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5.0,
                                  ),
                                ],
                              ),
                            ),
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
  }

  openAlertBoxConnect(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              contentPadding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              backgroundColor: Color(0xFFF2F2F2),
              content: Container(
                width: size.width * 1 - 32,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Bog\'lanish',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          CloseButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                      Container(
                        child: Column(
                          children: [
                            RaisedButton(
                              onPressed: () {
                                _launchURL('tel:${contact.tel}');
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                height: 50,
                                width: size.width - 48,
                                child: Center(
                                    child: Text(
                                  "Qo'ng'iroq qilish",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                              ),
                              padding: EdgeInsets.all(0),
                              color: Color(0xFFFFE030),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            RaisedButton(
                              onPressed: () {
                                _launchURL(contact.tg);
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                height: 50,
                                width: size.width - 48,
                                child: Center(
                                    child: Text(
                                  "Telegram",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                              ),
                              padding: EdgeInsets.all(0),
                              color: Color(0xFFFFE030),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  openAlertBoxClose(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              contentPadding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              backgroundColor: Color(0xFFF2F2F2),
              content: Container(
                width: size.width - 32,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Qancha vaqt ishlamaysiz?',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          CloseButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(right: 20, left: 10, bottom: 10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: RaisedButton(
//                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    onPressed: () {
                                      minIndex == 7200
                                          ? setState(() {
                                              _click(0);
                                            })
                                          : setState(() {
                                              _click(7200);
                                            });
                                    },
                                    color: minIndex == 7200
                                        ? Color(0xFFFFE030)
                                        : Color(0xFFFFFFFF),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Container(
                                      height: 40,
                                      child: Center(
                                          child: Text(
                                        '2 soat',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: RaisedButton(
//                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    onPressed: () {
                                      minIndex == 99
                                          ? setState(() {
                                              _click(0);
                                            })
                                          : setState(() {
                                              _click(99);
                                            });
                                    },
                                    color: minIndex == 99
                                        ? Color(0xFFFFE030)
                                        : Color(0xFFFFFFFF),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Container(
                                      height: 40,
                                      child: Center(
                                          child: Text(
                                        'Ochiq',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: RaisedButton(
//                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    onPressed: () {
                                      minIndex == 86400
                                          ? setState(() {
                                              _click(0);
                                            })
                                          : setState(() {
                                              _click(86400);
                                            });
                                    },
                                    color: minIndex == 86400
                                        ? Color(0xFFFFE030)
                                        : Color(0xFFFFFFFF),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Container(
                                      height: 40,
                                      child: Center(
                                          child: Text(
                                        'Ertagacha',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: RaisedButton(
//                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    onPressed: () {
                                      minIndex == 111
                                          ? setState(() {
                                              _click(0);
                                            })
                                          : setState(() {
                                              _click(111);
                                            });
                                    },
                                    color: minIndex == 111
                                        ? Color(0xFFFFE030)
                                        : Color(0xFFFFFFFF),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Container(
                                      height: 40,
                                      child: Center(
                                          child: Text(
                                        'Noma\'lum',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (minIndex != 0) {
                            setState(() {
                              _isLoading = true;
                            });
                            Provider.of<Auth>(context, listen: false)
                                .setClosedDate(closeTime)
                                .then((_) {
                              setState(() {
                                _isLoading = false;
                                _isInit = true;
                              });
                              didChangeDependencies();
                              Navigator.of(context).pop();
                            });
                            minIndex = 0;
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          decoration: BoxDecoration(
                            color: minIndex != 0
                                ? Color(0xFFFFE030)
                                : Color(0xFFD8D8D8), //Color(0xFFD8D8D8),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5.0),
                                bottomRight: Radius.circular(5.0)),
                          ),
                          child: Text(
                            "Saqlash",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  void _click(int clickIndex) {
    setState(() {
      minIndex = clickIndex;
    });

    switch (clickIndex) {
      case 99:
        setState(() {
          closeTime = 0;
        });
        break;
      case 111:
        setState(() {
          closeTime = -1;
        });
        break;
      case 7200:
        setState(() {
          closeTime = 7200;
        });
        break;
      case 86400:
        setState(() {
          closeTime = 86400;
        });
        break;
    }
  }

  void _launchURL(String url) async {
//    const url1 = '${url}'
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
