import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';

class AuthScreen extends StatefulWidget {
  static const routeName = '/auth';

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with SingleTickerProviderStateMixin{




  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  final GlobalKey<FormState> _formKey = GlobalKey();

  var _fcm_token = "";

//  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {
    'username': '',
    'password': '',
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();


  @override
  void initState() {
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      setState(() {
        _fcm_token = token;
      });
      // print("Push Messaging token:  $_homeScreenText");
    });
  }

  Future<void> _submit() async {

    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(sound: true, badge: true, alert: true));
    // String clientToken = _firebaseMessaging.getToken().then((token) {
    //   // print("Token Init: " + token.toString());
    // }
    // ).toString();


    if (!_formKey.currentState.validate() && !_fcm_token.isNotEmpty) {
      // Invalid!
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });

    try{
      await Provider.of<Auth>(context, listen: false)
          .login(_authData['username'], _authData['password'], _fcm_token);
    }catch(error){
      print(error);
    }


    setState(() {
      _isLoading = false;
    });
  }



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            width: size.width - 32,
            margin: EdgeInsets.only(left: 16, top: size.height/4),
            child: Column(
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Login'),
                      SizedBox(height: 5,),
                      TextFormField(
                        validator: (value){
                          if(value.isEmpty){
                            return 'sdsds';
                          }
                        },
                        onSaved: (value) {
                          _authData['username'] = value;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Aroma",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)
                          ),
                        ),

                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Password'),
                      SizedBox(height: 5,),
                      TextFormField(
                        obscureText: true,
                        controller: _passwordController,
                        // ignore: missing_return
                        validator: (value) {
                          if (value.isEmpty || value.length < 5) {
                            return 'Password is too short!';
                          }
                        },
                        onSaved: (value) {
                          _authData['password'] = value;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "********",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                if (_isLoading)
                  CircularProgressIndicator()
                else
                RaisedButton(
                  onPressed: _submit,
                  child: Container(
                    height: 50,
                    width: size.width - 32,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                        child: Text(
                          "Kirish",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
//                shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(2),
//                ),
                  padding: EdgeInsets.all(0),
                  color: Color(0xFFFFE030),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
