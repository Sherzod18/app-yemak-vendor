import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/order.dart' show Order;
import '../widgets/order_item.dart';

class OrdersScreen extends StatefulWidget {
  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  Future<void> _refreshProducts(BuildContext context) async {
    print('Ishladi -------------');
    await Provider.of<Order>(context, listen: false).getAllOrders();
  }

  @override
  void initState() {

    super.initState();
  }

  @override
  void didChangeDependencies()  {

    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(OrdersScreen oldWidget) {
    print("didUpdateWidget +++++++++++++++");
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: Provider.of<Order>(context, listen: false).getAllOrders(),
      builder: (ctx, snapshot) =>
          snapshot.connectionState == ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Consumer<Order>(
                    builder: (ctx, orderData, child) => Container(
                      child: orderData.orders.length == 0? ListView(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: size.height*0.3),
                            child: Center(child: Icon(Icons.schedule, size: 80, color: Colors.black26,)),),
                          Container(
                            child: Center(child: Text('Hozircha buyurtmalar yuq,', style: TextStyle(color: Colors.black26),)),),
                          Container(
                            child: Center(child: Text("buyurtma bo'lishi bilan sizga xabar keladi.", style: TextStyle(color: Colors.black26),)),)
                        ],
                      ) : GridView(
                        padding: const EdgeInsets.only(
                            top: 20, left: 16, right: 16, bottom: 30),
                        children: orderData.orders
                            .map((order) => OrderItem(
                                order.id,
                                order.number,
                                '12',
                                '12',
                                '${order.customer == null ? "null" : order.customer.first_name} ${order.customer == null ? "null" : order.customer.last_name}',
                                '${order.cost_of_products}'))
                            .toList(),
                        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 200,
                          childAspectRatio: 1,
                          crossAxisSpacing: 12,
                          mainAxisSpacing: 12,
                        ),
                      ),
                    ),
                  ),
                ),
    );
  }
}
