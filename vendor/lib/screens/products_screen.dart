import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../providers/order.dart';
import '../models/history_root.dart';

class ProductsScreen extends StatefulWidget {
  static const routeName = '/products';

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  var _isLoading = false;
  var _isInit = true;
  List<HistoryRoot> roots;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      Provider.of<Order>(context, listen: false).getProducts().then((_) {
        roots = Provider.of<Order>(context, listen: false).products;
        setState(() {
          _isLoading = false;
        });
      });
    }

    _isInit = false;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: [
              Text(
                'Yemak',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Restaurant',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: SvgPicture.asset('assets/images/SplashCentreLogoIcon.svg'),
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                width: size.width - 32,
                margin: EdgeInsets.only(left: 16, top: 24),
                child: Column(
                  children: roots.map((root) {
                    return  Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${root.title}',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Column(
                          children: root.products.map((product){
                            return Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10, top: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        '${product.name == null ? 0 : product.name}',
                                        style: TextStyle(
                                            fontSize: 14, fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        '${product.price == null ? 0 : product.price} so\'m',
                                        style: TextStyle(
                                            fontSize: 14, fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Column(
                                    children: product.options.map((option){
                                      return Column(
                                        children: option.rows.map((row) {
                                          return Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                '${row.name}:',
                                                style: TextStyle(
                                                    fontSize: 14, color: Colors.black38),
                                              ),
                                              Text(
                                                '+${row.price == null ? 0 : row.price} so\'m',
                                                style: TextStyle(
                                                    fontSize: 14, color: Colors.black38),
                                              )
                                            ],
                                          );
                                        }).toList(),
                                      );
                                    }).toList(),
                                  ),
                                ),
                                Divider(
                                  height: 12,
                                ),
                              ],
                            );
                          } ).toList(),
                        ),
                      ],
                    );
                  }).toList(),
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: null,
        child: Icon(Icons.filter_list),
        backgroundColor: Color(0xFFFFE030),
      ),
    );
  }
}
