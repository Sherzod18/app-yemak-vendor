import 'package:flutter/material.dart';

import '../screens/order_details_screen.dart';

class OrderItem extends StatelessWidget {
  final int id;
  final String number;
  final String timeFrom;
  final String timeTo;
  final String fullName;
  final String price;

  OrderItem(
    this.id,
    this.number,
    this.timeFrom,
    this.timeTo,
    this.fullName,
    this.price,
  );

  void selectCategory(BuildContext ctx, int id) {
    Navigator.of(ctx).pushNamed(
      OrderDetailsScreen.routeName,
      arguments: {
        'id': id,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectCategory(context, id),
      borderRadius: BorderRadius.circular(5),
      splashColor: Theme.of(context).primaryColor,
      hoverColor: Colors.green,
      child: Container(
        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 5.0,
            ),
          ],
        ),
        child: Column(
          children: [
            Center(
                child: Text(
              'Buyurtma #${number}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            )),
            SizedBox(
              height: 10,
            ),
            Text(
              'from: ${timeFrom}',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w100),
            ),
            Text(
              'to: ${timeTo}',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w100),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Mijoz:',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w100),
            ),
            Text(
              '${fullName}',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              '${price} so\'m',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }
}
