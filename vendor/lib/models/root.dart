import './product.dart';
import './restaurant.dart';
import './customer.dart';
import './destination.dart';
import './driver.dart';

class Root {
  int id;
  String number;
  Restaurant restaurant;
  List<Product> products;
  Driver driver;
  Customer customer;
  Destination destination;
  int cost_of_products;
  int cost_of_delivery;
  Object additional_information;
  int order_time;
  int status;
  int payment_type;
  int step;
  int cooking_time;
  Object scheduled_time;
  bool is_cancelled;
  Object cancel_time;
  Object cancel_reason;
  int created_at;

  Root({
    this.id,
    this.number,
    this.restaurant,
    this.products,
    this.driver,
    this.customer,
    this.destination,
    this.cost_of_products,
    this.cost_of_delivery,
    this.additional_information,
    this.order_time,
    this.status,
    this.payment_type,
    this.step,
    this.cooking_time,
    this.scheduled_time,
    this.is_cancelled,
    this.cancel_time,
    this.cancel_reason,
    this.created_at
  });

  @override
  String toString() {
    return 'Root{id: $id, number: $number, restaurant: $restaurant, products: $products, driver: $driver, customer: $customer, destination: $destination, cost_of_products: $cost_of_products, cost_of_delivery: $cost_of_delivery, additional_information: $additional_information, order_time: $order_time, status: $status, payment_type: $payment_type, step: $step, cooking_time: $cooking_time, scheduled_time: $scheduled_time, is_cancelled: $is_cancelled, cancel_time: $cancel_time, cancel_reason: $cancel_reason, created_at: $created_at}';
  }


}
