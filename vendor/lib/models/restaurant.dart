
class Restaurant {
  int id;
  String name;
  String description;
  String photo;
  String logotype;
  List<String> tags;

  Restaurant({
    this.id,
    this.name,
    this.description,
    this.photo,
    this.logotype,
    this.tags
  });
}
