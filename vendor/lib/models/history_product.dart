import './history_option.dart';

class HistoryProduct {
  int id;
  String name;
  String description;
  String photo;
  int price;
  List<HistoryOption> options;
  int estimated_cooking_time;

  HistoryProduct({this.id, this.name, this.description, this.photo, this.price,
    this.options, this.estimated_cooking_time});


}