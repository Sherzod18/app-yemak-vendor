import './option.dart';

class Product {
  int id;
  String name;
  String description;
  String photo;
  int price;
  int amount;
  List<Option> options;

  Product({
    this.id,
    this.name,
    this.description,
    this.photo,
    this.price,
    this.amount,
    this.options,
  });

  @override
  String toString() {
    return 'Product{id: $id, name: $name, description: $description, photo: $photo, price: $price, amount: $amount, options: $options}';
  }


}
