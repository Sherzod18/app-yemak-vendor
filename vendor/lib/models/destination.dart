class Destination {
  double lat;
  double long;
  Object address;
  String maps_image;

  Destination({
    this.lat,
    this.long,
    this.address,
    this.maps_image,
  });
}
