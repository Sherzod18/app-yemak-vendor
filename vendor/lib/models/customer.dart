class Customer {
  int id;
  String first_name;
  String last_name;
  String phone_number;

  Customer({
    this.id,
    this.first_name,
    this.last_name,
    this.phone_number,
  });
}
