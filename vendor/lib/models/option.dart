import './row2.dart';

class Option {
  int id;
  String name;
  Row2 row;

  Option({
    this.id,
    this.name,
    this.row,
  });

  @override
  String toString() {
    return 'Option{id: $id, name: $name, row: $row}';
  }


}
