import './history_product.dart';

class HistoryRoot {
  int id;
  String title;
  List<HistoryProduct> products;

  HistoryRoot({this.id, this.title, this.products});

}