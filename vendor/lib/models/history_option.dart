import './history_row.dart';

class HistoryOption {
  int id;
  String name;
  List<HistoryRow> rows;

  HistoryOption({this.id, this.name, this.rows});


}