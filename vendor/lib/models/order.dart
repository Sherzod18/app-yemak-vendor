import './order_detail.dart';
import 'package:flutter/material.dart';

class Order {
  final String id;
  final String timeFrom;
  final String timeTo;
  final String fullName;
  final double price;
  final List<OrderDetail> orderDetails;

  const Order({
    this.id,
    this.timeFrom,
    this.timeTo,
    this.fullName,
    this.price,
    this.orderDetails,
  });
}
