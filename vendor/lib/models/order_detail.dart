class OrderDetail {
  final int count;
  final String name;
  final int price;
  final List<String> amounts;

  const OrderDetail({
    this.count,
    this.name,
    this.price,
    this.amounts,
  });
}
