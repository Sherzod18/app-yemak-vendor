import 'dart:convert';
import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/constants.dart';
import '../models/work_time.dart';
import '../models/contact.dart';
import '../models/restaurant.dart';
import '../models/temporarily.dart';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  int _userId;
  Timer _authTimer;
  Restaurant restaurant;
  Restaurant _restaurant;
  Temporarily _temporarily;
  Contact _contact;
  WorkTime _workTime;
  bool isProd;

  bool get isAuth {
    return token != null;
  }

  Contact get getContact{
    return _contact;
  }

  Temporarily get getTemporarily{
    return _temporarily;
  }

  WorkTime get getWorkTime {
    return _workTime;
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  String get userId {
    return _userId.toString();
  }

  Restaurant get getRestaran{
    return restaurant;
  }

  Future<void> _authenticate(
      String username, String password,String fcm_token, String urlSegment) async {
    print("---TOKEN ---   $fcm_token");
    final url = '${Constants.TEST_URL}$urlSegment';
    try {
      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'X-Device-Token': fcm_token
        },
        body: jsonEncode(
            <String, String>{'username': username, 'password': password}),
      );

      final resData = jsonDecode(response.body);

      if (resData['ok']) {
        _token = resData['data']['access_token'];
        _userId = resData['data']['id'];
        _expiryDate = DateTime.now().add(
          Duration(seconds: 86400),
        );
        _autoLogout();
        notifyListeners();
        final prefs = await SharedPreferences.getInstance();
        final userData = json.encode(
          {
            'token': _token,
            'userId': _userId,
            'expiryDate': _expiryDate.toIso8601String(),
          },
        );

        prefs.setString('userData', userData);

        print(resData['ok']);
        print(resData['data']);
        print('_token - ' + _token);
      }
    } catch (error) {
      print(error);
    }
  }

  Future<void> login(String username, String password, String fcm_token) async {
    const urlSegment = 'auth';
    print(username + " --------- " + password + " --------------- " + fcm_token);

    return _authenticate(username, password,fcm_token, urlSegment);
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }

    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = expiryDate;
    notifyListeners();
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('userData');
    prefs.clear();
  }

  //Restoran ma'lumotlarini olib kelish
  Future<void> getAccount() async {
    var url = "${Constants.TEST_URL}account";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final resData = jsonDecode(response.body);

      if (resData['ok']) {
        final data = resData['data']['restaurant'];
        final contact = resData['data']['contacts'];
        final workTime = resData['data']['work_time'];
        // ignore: unnecessary_statements
        var timer = resData['data']['temporarily_closed_until'] as int;


        final tags = resData['data']['restaurant']['tags'] as List<dynamic>;

        List<String> tagList = [];

        tags.forEach((element) {
          tagList.add(element);
        });

        _restaurant = Restaurant(
          name: data['name'],
          description: data['description'],
          photo: data['photo'],
          tags: tagList,
        );
        _contact = Contact(
          tel: contact['tel'],
          tg: contact['tg']
        );

        _workTime = WorkTime(
          from: workTime['from'],
          to: workTime['to']
        );

        _temporarily = Temporarily(
            temporarily_closed_until: timer
        );

      }
      restaurant = _restaurant;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }

  //Restoranni yopiq deb belgilash
  Future<void> setClosedDate(int until) async{
    var url = "${Constants.TEST_URL}account/closed?until=$until";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final resData = jsonDecode(response.body);

      if (resData['ok']) {

      }
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
