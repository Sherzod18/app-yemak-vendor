import 'dart:convert';
import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

import '../utils/constants.dart';
import '../models/history_option.dart';
import '../models/history_product.dart';
import '../models/history_row.dart';
import '../models/history_root.dart';
import '../models/restaurant.dart';
import '../models/root.dart';
import '../models/product.dart';
import '../models/option.dart';
import '../models/row2.dart';
import '../models/customer.dart';
import '../models/destination.dart';
import '../models/driver.dart';

class Order with ChangeNotifier {
  bool isProd;
  String _token;
  List<Root> _items = [];

  List<Root> _allAcceptOrders = [];

  List<Root> _allSuccessOrders = [];

  List<Root> _allHistoryOrders = [];

  List<Root> loadedData = [];

  List<HistoryRoot> _allProducts = [];

  List<HistoryRoot> loadedDataProducts = [];


  Order(this.isProd);

  List<HistoryRoot> get products {
    return _allProducts;
  }

  List<Root> get orders {
    return _items;
  }

  List<Root> get acceptOrders {
    return _allAcceptOrders;
  }

  List<Root> get successOrders {
    return _allSuccessOrders;
  }

  List<Root> get historyOrders {
    return _allHistoryOrders;
  }

  void clearItem(){
    _items = [];
  }

  // Yangi kelib tushgan buyurtmalar
  Future<void> getAllOrders() async {
    var url =
        "${Constants.TEST_URL}order?pageSize=10000&pageNumber=0&step=1";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes) {
        final resData =
            jsonDecode(response.body)['data']['orders'] as List<dynamic>;

        resData.forEach(
          (order) {
            final products = order['products'] as List<dynamic>;
            List<Product> productList = [];

            products.forEach((product) {
              final options = product['options'] as List<dynamic>;
              List<Option> optionList = [];

              options.forEach((option) {
                optionList.add(Option(
                    id: option['id'],
                    name: option['name'],
                    row: Row2(
                      id: option['row']['id'],
                      name: option['row']['name'],
                      price: option['row']['price'],
                    )));
              });

              productList.add(Product(
                  id: product['id'],
                  name: product['name'],
                  description: product['description'],
                  photo: product['photo'],
                  price: product['price'],
                  amount: product['amount'],
                  options: optionList));
            });

            loadedData.add(
              Root(
                id: order['id'],
                number: order['number'],
                restaurant: Restaurant(
                  id: order['restaurant']['id'],
                  name: order['restaurant']['name'],
                  description: order['restaurant']['description'],
                  photo: order['restaurant']['photo'],
                  logotype: order['restaurant']['logotype'],
                ),
                products: productList,
                driver: order['driver'] != null
                    ? Driver(
                        id: order['driver']['id'],
                        first_name: order['driver']['first_name'],
                        last_name: order['driver']['last_name'],
                        phone_number: order['driver']['phone_number'],
                        car: order['driver']['car'],
                      )
                    : null,
                customer: order['customer'] != null
                    ? Customer(
                        id: order['customer']['id'],
                        first_name: order['customer']['first_name'],
                        last_name: order['customer']['last_name'],
                        phone_number: order['customer']['phone_number'],
                      )
                    : null,
                destination: order['destination'] != null
                    ? Destination(
                        lat: order['destination']['lat'],
                        long: order['destination']['long'],
                        address: order['destination']['address'],
                        maps_image: order['destination']['maps_image'],
                      )
                    : null,
                cost_of_products: order['cost_of_products'],
                cost_of_delivery: order['cost_of_delivery'],
                additional_information: order['additional_information'],
                order_time: order['order_time'],
                status: order['status'],
                payment_type: order['payment_type'],
                step: order['step'],
                cooking_time: order['cooking_time'],
                scheduled_time: order['scheduled_time'],
                is_cancelled: order['is_cancelled'],
                cancel_time: order['cancel_time'],
                cancel_reason: order['cancel_reason'],
                created_at: order['created_at'],
              ),
            );
          },
        );
      }

      _items = loadedData;
      loadedData = [];
      notifyListeners();
    } catch (error) {
      print(error);
    }

    notifyListeners();
  }

  // id buyicha 1 ta buyurtmani olish
  Root getById(int id) {
    return _items.firstWhere((root) => root.id == id);
  }

  Root getByIdAccept(int id) {
    return _allAcceptOrders.firstWhere((root) => root.id == id);
  }

  Root getByIdSuccess(int id) {
    return _allSuccessOrders.firstWhere((root) => root.id == id);
  }

  // Buyurtmani qabul qilish
  Future<void> acceptOrder(int id, int time) async {
    var url = "${Constants.TEST_URL}order/accept";

    var time2 = (DateTime.now()
                .add(Duration(seconds: time * 60))
                .millisecondsSinceEpoch /
            1000)
        .floor();

    final prefs = await SharedPreferences.getInstance();

    if (!prefs.containsKey('userData')) {
      return false;
    }

    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
        body: jsonEncode(<String, int>{
          'order_id': id,
          'cooking_time': time2,
        }),
      );

      final resData = jsonDecode(response.body);

      if (resData['ok']) {
//        getAllOrders();
      }
      notifyListeners();

    } catch (error) {
      print(error);
    }
  }

  //Tayyor bo'lish vaqtini o'zgartirish
  Future<void> editedTime(int id, int time, int scheduled_time) async {

    var time2 = time * 60 + scheduled_time;

    /*(DateTime.now()
        .add(Duration(seconds: time * 60))
        .millisecondsSinceEpoch /
        1000)
        .floor();*/

    var url = "${Constants.TEST_URL}order/extend-time?order_id=$id&time=$time2";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {

      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final resData = jsonDecode(response.body);

      if (resData['ok']) {
        getAllOrders();
      }
      notifyListeners();

    }catch(error){
      throw error;
    }


  }

  // Buyurtmani bekor qilish
  Future<void> rejectOrder(String id, String reason)async{
    var url = "${Constants.TEST_URL}order/reject";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try{
      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
        body: jsonEncode(<String, String>{
          'order_id': id,
          'reason': reason,
        }),
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes) {
//        getAllOrders();
      }
      notifyListeners();
    }catch(error){
      throw error;
    }
  }

  // timestamp ni vaqtga ugirish methodi
  String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {
        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }

    return time;
  }

  // Tayyorlanayotgan buyurtmalar
  Future<void> getAllAcceptOrders() async {
    var url =
        "${Constants.TEST_URL}order?pageSize=10000&pageNumber=0&step=2";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes) {
        final resData =
        jsonDecode(response.body)['data']['orders'] as List<dynamic>;

        resData.forEach(
              (order) {
            final products = order['products'] as List<dynamic>;
            List<Product> productList = [];

            products.forEach((product) {
              final options = product['options'] as List<dynamic>;
              List<Option> optionList = [];

              options.forEach((option) {
                optionList.add(Option(
                    id: option['id'],
                    name: option['name'],
                    row: Row2(
                      id: option['row']['id'],
                      name: option['row']['name'],
                      price: option['row']['price'],
                    )));
              });

              productList.add(Product(
                  id: product['id'],
                  name: product['name'],
                  description: product['description'],
                  photo: product['photo'],
                  price: product['price'],
                  amount: product['amount'],
                  options: optionList));
            });

            loadedData.add(
              Root(
                id: order['id'],
                number: order['number'],
                restaurant: Restaurant(
                  id: order['restaurant']['id'],
                  name: order['restaurant']['name'],
                  description: order['restaurant']['description'],
                  photo: order['restaurant']['photo'],
                  logotype: order['restaurant']['logotype'],
                ),
                products: productList,
                driver: order['driver'] != null
                    ? Driver(
                  id: order['driver']['id'],
                  first_name: order['driver']['first_name'],
                  last_name: order['driver']['last_name'],
                  phone_number: order['driver']['phone_number'],
                  car: order['driver']['car'],
                )
                    : null,
                customer: order['customer'] != null
                    ? Customer(
                  id: order['customer']['id'],
                  first_name: order['customer']['first_name'],
                  last_name: order['customer']['last_name'],
                  phone_number: order['customer']['phone_number'],
                )
                    : null,
                destination: order['destination'] != null
                    ? Destination(
                  lat: order['destination']['lat'],
                  long: order['destination']['long'],
                  address: order['destination']['address'],
                  maps_image: order['destination']['maps_image'],
                )
                    : null,
                cost_of_products: order['cost_of_products'],
                cost_of_delivery: order['cost_of_delivery'],
                additional_information: order['additional_information'],
                order_time: order['order_time'],
                status: order['status'],
                payment_type: order['payment_type'],
                step: order['step'],
                cooking_time: order['cooking_time'],
                scheduled_time: order['scheduled_time'],
                is_cancelled: order['is_cancelled'],
                cancel_time: order['cancel_time'],
                cancel_reason: order['cancel_reason'],
                created_at: order['created_at'],
              ),
            );
          },
        );
      }

      _allAcceptOrders = loadedData;
      loadedData = [];
      notifyListeners();
    } catch (error) {
      print(error);
    }

    notifyListeners();
  }

  // Tayyor bulgan buyurtmalar
  Future<void> getAllSuccessOrders() async {
    var url =
        "${Constants.TEST_URL}order?pageSize=10000&pageNumber=0&step=3";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try {
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes) {
        final resData =
        jsonDecode(response.body)['data']['orders'] as List<dynamic>;

        resData.forEach(
              (order) {
            final products = order['products'] as List<dynamic>;
            List<Product> productList = [];

            products.forEach((product) {
              final options = product['options'] as List<dynamic>;
              List<Option> optionList = [];

              options.forEach((option) {
                optionList.add(Option(
                    id: option['id'],
                    name: option['name'],
                    row: Row2(
                      id: option['row']['id'],
                      name: option['row']['name'],
                      price: option['row']['price'],
                    )));
              });

              productList.add(Product(
                  id: product['id'],
                  name: product['name'],
                  description: product['description'],
                  photo: product['photo'],
                  price: product['price'],
                  amount: product['amount'],
                  options: optionList));
            });

            loadedData.add(
              Root(
                id: order['id'],
                number: order['number'],
                restaurant: Restaurant(
                  id: order['restaurant']['id'],
                  name: order['restaurant']['name'],
                  description: order['restaurant']['description'],
                  photo: order['restaurant']['photo'],
                  logotype: order['restaurant']['logotype'],
                ),
                products: productList,
                driver: order['driver'] != null
                    ? Driver(
                  id: order['driver']['id'],
                  first_name: order['driver']['first_name'],
                  last_name: order['driver']['last_name'],
                  phone_number: order['driver']['phone_number'],
                  car: order['driver']['car'],
                )
                    : null,
                customer: order['customer'] != null
                    ? Customer(
                  id: order['customer']['id'],
                  first_name: order['customer']['first_name'],
                  last_name: order['customer']['last_name'],
                  phone_number: order['customer']['phone_number'],
                )
                    : null,
                destination: order['destination'] != null
                    ? Destination(
                  lat: order['destination']['lat'],
                  long: order['destination']['long'],
                  address: order['destination']['address'],
                  maps_image: order['destination']['maps_image'],
                )
                    : null,
                cost_of_products: order['cost_of_products'],
                cost_of_delivery: order['cost_of_delivery'],
                additional_information: order['additional_information'],
                order_time: order['order_time'],
                status: order['status'],
                payment_type: order['payment_type'],
                step: order['step'],
                cooking_time: order['cooking_time'],
                scheduled_time: order['scheduled_time'],
                is_cancelled: order['is_cancelled'],
                cancel_time: order['cancel_time'],
                cancel_reason: order['cancel_reason'],
              ),
            );
          },
        );
      }

      _allSuccessOrders = loadedData;
      loadedData = [];
      notifyListeners();
    } catch (error) {
      print(error);
    }

    notifyListeners();
  }

  // tayyor bulgan bururtmani yuborish
  Future<void> orderReady(int id) async {
    var url =
        "${Constants.TEST_URL}order/ready?order_id=$id";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    final http.Response response = await http.get(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': _token,
      },
    );

    final isRes = jsonDecode(response.body)['ok'] as bool;

    if (isRes){

    }

    try{

    }catch(error){
      throw error;
    }

  }

  // Buyurtmalar tarixi
  Future<void> getHistoryOrders() async{
    var url =
        "${Constants.TEST_URL}order/history";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];


    try{
      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes) {
        final resData =
        jsonDecode(response.body)['data']['orders'] as List<dynamic>;

        resData.forEach(
              (order) {
            final products = order['products'] as List<dynamic>;
            List<Product> productList = [];

            products.forEach((product) {
              final options = product['options'] as List<dynamic>;
              List<Option> optionList = [];

              options.forEach((option) {
                optionList.add(Option(
                    id: option['id'],
                    name: option['name'],
                    row: Row2(
                      id: option['row']['id'],
                      name: option['row']['name'],
                      price: option['row']['price'],
                    )));
              });

              productList.add(Product(
                  id: product['id'],
                  name: product['name'],
                  description: product['description'],
                  photo: product['photo'],
                  price: product['price'],
                  amount: product['amount'],
                  options: optionList));
            });

            loadedData.add(
              Root(
                id: order['id'],
                number: order['number'],
                restaurant: Restaurant(
                  id: order['restaurant']['id'],
                  name: order['restaurant']['name'],
                  description: order['restaurant']['description'],
                  photo: order['restaurant']['photo'],
                  logotype: order['restaurant']['logotype'],
                ),
                products: productList,
                driver: order['driver'] != null
                    ? Driver(
                  id: order['driver']['id'],
                  first_name: order['driver']['first_name'],
                  last_name: order['driver']['last_name'],
                  phone_number: order['driver']['phone_number'],
                  car: order['driver']['car'],
                )
                    : null,
                customer: order['customer'] != null
                    ? Customer(
                  id: order['customer']['id'],
                  first_name: order['customer']['first_name'],
                  last_name: order['customer']['last_name'],
                  phone_number: order['customer']['phone_number'],
                )
                    : null,
                destination: order['destination'] != null
                    ? Destination(
                  lat: order['destination']['lat'],
                  long: order['destination']['long'],
                  address: order['destination']['address'],
                  maps_image: order['destination']['maps_image'],
                )
                    : null,
                cost_of_products: order['cost_of_products'],
                cost_of_delivery: order['cost_of_delivery'],
                additional_information: order['additional_information'],
                order_time: order['order_time'],
                status: order['status'],
                payment_type: order['payment_type'],
                step: order['step'],
                cooking_time: order['cooking_time'],
                scheduled_time: order['scheduled_time'],
                is_cancelled: order['is_cancelled'],
                cancel_time: order['cancel_time'],
                cancel_reason: order['cancel_reason'],
                created_at: order['created_at'],
              ),
            );
          },
        );
      }

      _allHistoryOrders = loadedData;
      loadedData = [];
      notifyListeners();

    }catch(error){
      throw error;
    }

  }

  // mahsulotlar
  Future<void> getProducts() async {
    var url =
        "${Constants.TEST_URL}product";
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
    json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];

    try{

      final http.Response response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept-Language': 'uz',
          'Authorization': _token,
        },
      );

      final isRes = jsonDecode(response.body)['ok'] as bool;

      if (isRes){
        final resData =
        jsonDecode(response.body)['data'] as List<dynamic>;

        resData.forEach((element) {
          final products = element['products'] as List<dynamic>;
          List<HistoryProduct> hisProducts = [];

          products.forEach((product) {
            final options = product['options'] as List<dynamic>;
            List<HistoryOption> hisOprions = [];

            options.forEach((option) {
              final rows = option['rows'] as List<dynamic>;
              List<HistoryRow> hisRows = [];

              rows.forEach((row) {
                hisRows.add(HistoryRow(
                    id: row['id'],
                    name: row['name'],
                    price: row['price']
                ));
              });

              hisOprions.add(HistoryOption(
                  id: option['id'],
                  name: option['name'],
                  rows: hisRows
              ));

            });

            hisProducts.add(HistoryProduct(
                id: product['id'],
                name: product['name'],
                price: product['price'],
                options: hisOprions,
                photo: product['photo'],
                description: product['description'],
                estimated_cooking_time: product['estimated_cooking_time']
            ));
          });

          loadedDataProducts.add(
              HistoryRoot(
                  id: element['id'],
                  title: element['title'],
                  products: hisProducts
              )
          );

        });

      }

      _allProducts = loadedDataProducts;
      loadedDataProducts = [];
      notifyListeners();

    }catch(error){
      throw error;
    }
  }
}
