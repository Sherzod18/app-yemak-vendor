import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import './screens/splash_screen.dart';
import './screens/tabs_screen.dart';
import './screens/order_details_screen.dart';
import './screens/home_item_screen.dart';
import './screens/success_item_screen.dart';
import './screens/order_history_screen.dart';
import './screens/order_history_item_screen.dart';
import './screens/products_screen.dart';
import './screens/auth_screen.dart';

import './providers/auth.dart';
import './providers/order.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isProd = false;

  String _statusText = "Waiting...";
  final String _finished = "Finished creating channel";
  final String _error = "Error while creating channel";

  FirebaseMessaging _fcm = FirebaseMessaging();


  @override
  void initState() {
    super.initState();

    _fcm.configure(
      onMessage: (message) async{
        print('onMessage Main ---- $message');
      },

        onResume: (message) async{
          print('onResume Main ---- $message');
        },

        onLaunch: (message) async{
          print('onLaunch Main ---- $message');
        }
    );
  }

  static const MethodChannel _channel =
  MethodChannel('somethinguniqueforyou.com/channel_test');

  Map<String, String> channelMap = {
    "id": "CHAT_MESSAGES",
    "name": "Chats",
    "description": "Chat notifications",
  };

  void _createNewChannel() async {
    try {
      await _channel.invokeMethod('createNotificationChannel', channelMap);
      setState(() {
        _statusText = _finished;
      });
    } on PlatformException catch (e) {
      _statusText = _error;
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // ignore: missing_required_param
        ChangeNotifierProvider<Auth>(
          builder:(ctx) => Auth(),

          //   create: (ctx) => Auth(),
        ),
        // ignore: missing_required_param
        ChangeNotifierProxyProvider<Auth, Order>(
          // ignore: deprecated_member_use
          builder: (ctx, auth, previuosProducts) => Order(isProd),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.amber,
            canvasColor: Color.fromRGBO(237, 237, 237, 1),
//        fontFamily: 'Raleway',
//        textTheme: ThemeData.light().textTheme.copyWith(
//          body1: TextStyle(
//            color: Color.fromRGBO(20, 51, 51, 1),
//          ),
//          body2: TextStyle(
//            color: Color.fromRGBO(20, 51, 51, 1),
//          ),
//          title: TextStyle(
//            fontSize: 22,
//            fontFamily: 'RobotoCondensed',
//            fontWeight: FontWeight.bold,
//          ),
//        ),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: auth.isAuth
              ? TabsScreen()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
//          initialRoute: '/',
//           default is '/'
          routes: {
//            '/': (ctx) => AuthScreen(),
            OrderDetailsScreen.routeName: (ctx) => OrderDetailsScreen(),
            HomeItemScreen.routeName: (ctx) => HomeItemScreen(),
            SuccessItemScreen.routeName: (ctx) => SuccessItemScreen(),
            OrderHistoryScreen.routeName: (ctx) => OrderHistoryScreen(),
            OrderHistoryItemScreen.routeName: (ctx) => OrderHistoryItemScreen(),
            ProductsScreen.routeName: (ctx) => ProductsScreen(),
//        AuthScreen.routeName: (ctx) => AuthScreen(),
          },
        ),
      ),
    );
  }
}
